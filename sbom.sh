#!/bin/bash

app_name='mvn_sbom'

app_id=$(curl -u admin:admin123 -X GET "http://localhost:8070/api/v2/applications?publicId=${app_name}" -s | jq -r .applications[].id)

echo "${app_name} -> ${app_id}"

cycdx_bom_file='target/cyclonedx-bom.xml'
spdx_bom_file='target/spdx-bom.xml'

mvn clean package clm:evaluate
curl -u admin:admin123 -X GET -H 'Accept: application/xml' -o ${cycdx_bom_file} http://localhost:8070/api/v2/cycloneDx/1.4/${app_id}/stages/build
curl -u admin:admin123 -X GET -H 'Accept: application/xml' -o ${spdx_bom_file} 'http://localhost:8070/api/v2/spdx/${app_id}/stages/build?format=xml'


